-- List of prefab files
PrefabFiles = {
    "warty",
    "warty_none",
    "frog_house",
    "leap_frogger"
}

-- Global variables
local _G = GLOBAL
local STRINGS = _G.STRINGS
local Ingredient = _G.Ingredient
local TECH = _G.TECH
local SpawnPrefab = _G.SpawnPrefab
local require = _G.require

-- Define assets used by the mod
Assets = {
    -- Portraits
    Asset("IMAGE", "images/saveslot_portraits/warty.tex"),
    Asset("ATLAS", "images/saveslot_portraits/warty.xml"),
    Asset("IMAGE", "images/selectscreen_portraits/warty.tex"),
    Asset("ATLAS", "images/selectscreen_portraits/warty.xml"),
    Asset("IMAGE", "images/selectscreen_portraits/warty_silho.tex"),
    Asset("ATLAS", "images/selectscreen_portraits/warty_silho.xml"),
    Asset("IMAGE", "bigportraits/warty.tex"),
    Asset("ATLAS", "bigportraits/warty.xml"),

    -- Map icons
    Asset("IMAGE", "images/map_icons/warty.tex"),
    Asset("ATLAS", "images/map_icons/warty.xml"),

    -- Avatars
    Asset("IMAGE", "images/avatars/avatar_warty.tex"),
    Asset("ATLAS", "images/avatars/avatar_warty.xml"),
    Asset("IMAGE", "images/avatars/avatar_ghost_warty.tex"),
    Asset("ATLAS", "images/avatars/avatar_ghost_warty.xml"),
    Asset("IMAGE", "images/avatars/self_inspect_warty.tex"),
    Asset("ATLAS", "images/avatars/self_inspect_warty.xml"),

    -- Names
    Asset("IMAGE", "images/names/names_warty.tex"),
    Asset("ATLAS", "images/names/names_warty.xml"),
    Asset("IMAGE", "images/names/names_gold_warty.tex"),
    Asset("ATLAS", "images/names/names_gold_warty.xml"),

    -- Inventory images
    Asset("IMAGE", "images/inventory_images/frog_house.tex"),
    Asset("ATLAS", "images/inventory_images/frog_house.xml"),
    Asset("IMAGE", "images/inventory_images/leap_frogger.tex"),
    Asset("ATLAS", "images/inventory_images/leap_frogger.xml"),

    -- Skilltree
    -- Asset( "IMAGE", "images/skilltree_images/warty_skilltree.tex" ),
    -- Asset( "ATLAS", "images/skilltree_images/warty_skilltree.xml" ),
}

-- Add minimap atlas
AddMinimapAtlas("images/map_icons/warty.xml")

-- Character select screen lines
STRINGS.CHARACTER_TITLES.warty = "Warty"
STRINGS.CHARACTER_NAMES.warty = "Warty"
STRINGS.CHARACTER_DESCRIPTIONS.warty = "*Perk 1\n*Perk 2\n*Perk 3"
STRINGS.CHARACTER_QUOTES.warty = "\"Quote\""
STRINGS.CHARACTER_SURVIVABILITY.warty = "Slim"

-- Custom speech strings
STRINGS.CHARACTERS.WARTY = require "speech_warty"

-- Character's name as appears in-game
STRINGS.NAMES.WARTY = "Warty"
STRINGS.SKIN_NAMES.warty_none = "Warty"

-- Skins shown in the cycle view window on the character select screen
local skin_modes = {
    {
        type = "ghost_skin",
        anim_bank = "ghost",
        idle_anim = "idle",
        scale = 0.75,
        offset = { 0, -25 }
    },
}

-- -- Skilltree
-- local SkillTreeDefs = require("prefabs/skilltree_defs")

-- local OldGetSkilltreeBG = GLOBAL.GetSkilltreeBG
-- function GLOBAL.GetSkilltreeBG(imagename, ...)
--     if imagename == "warty_background.tex" then
--         return "images/skilltree_images/warty_skilltree.xml"
--     else
--         return OldGetSkilltreeBG(imagename, ...)
--     end
-- end

-- local CreateSkillTree = function()
--     print("Creating a skilltree for Warty")
--     local BuildSkillsData = require("prefabs/skilltree_warty") -- Load in the skilltree

--     if BuildSkillsData then
--         local data = BuildSkillsData(SkillTreeDefs.FN)

--         if data then
--             SkillTreeDefs.CreateSkillTreeFor("warty", data.SKILLS)
--             SkillTreeDefs.SKILLTREE_ORDERS["warty"] = data.ORDERS
--             print("Created Warty skilltree")
--         end
--     end
-- end
-- CreateSkillTree();

-- Skilltree end

-- Add mod character to mod character list with gender specification
AddModCharacter("warty", "MALE", skin_modes)

-- Define strings for frog house
STRINGS.NAMES.FROG_HOUSE = "Frog house"
STRINGS.RECIPE_DESC.FROG_HOUSE = "It's a frog house!"

-- Define strings for frog house
STRINGS.NAMES.LEAP_FROGGER = "Leapfrogger"
STRINGS.RECIPE_DESC.LEAP_FROGGER = "It's a frog bat basically.."

-- Create a character-specific recipe for frog house
local frog_house_recipe_config = {
    atlas = "images/inventory_images/frog_house.xml",
    builder_tag = "warty",
    product = "frog_house",
}

-- Create a character-specific recipe for leap frogger
local leap_frogger_recipe_config = {
    atlas = "images/inventory_images/leap_frogger.xml",
    builder_tag = "warty",
    product = "leap_frogger",
}

AddCharacterRecipe("frog_house", { Ingredient("cutgrass", 1) }, TECH.NONE, frog_house_recipe_config, nil)
AddCharacterRecipe("leap_frogger", { Ingredient("twigs", 1) }, TECH.NONE, leap_frogger_recipe_config, nil)

-- Add recipe for frog house
AddRecipe2("frog_house",
        {
            Ingredient("cutgrass", 1),
            Ingredient("goldnugget", 1),
        },
        TECH.SCIENCE_ONE,
        {
            product = "frog_house",
            placer = "frog_house_placer",
            atlas = "images/inventory_images/frog_house.xml"
        }
)

-- Add recipe for leap_frogger
AddRecipe2("leap_frogger",
        {
            Ingredient("twigs", 1),
        },
        TECH.SCIENCE_ONE,
        {
            product = "leap_frogger",
            atlas = "images/inventory_images/leap_frogger.xml",
        }
)

RegisterInventoryItemAtlas("images/inventory_images/leap_frogger.xml", "images/inventory_images/leap_frogger.tex")

-- Function to spawn frogs periodically near the frog house
local function SpawnFrog(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local frog = _G.SpawnPrefab("frog")
    frog.Transform:SetPosition(x, y, z)
    local num_frogs_spawned = 0
    num_frogs_spawned = num_frogs_spawned + 1
    if num_frogs_spawned >= 2 then
        inst:DoTaskInTime(5, function()
            num_frogs_spawned = 0
        end)
    end
end

-- Post-init function for frog house prefab
local function frog_house_postinit(inst)
    -- Add workable component
    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(_G.ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(1)
    inst.components.workable:SetOnFinishCallback(function(inst)
        inst.AnimState:PlayAnimation("hit")
        local collapse_fx = SpawnPrefab("collapse_small")
        collapse_fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        inst:DoTaskInTime(0.5, function()
            inst:Remove()
        end)
    end)

    -- Listen for built event to spawn frogs
    inst:ListenForEvent("onbuilt", function()
        SpawnFrog(inst)
        SpawnFrog(inst)
    end)
end

-- Add post-init function to frog house prefab
AddPrefabPostInit("frog_house", frog_house_postinit)
