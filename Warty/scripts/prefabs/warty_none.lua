-- Define assets used by the skin
local assets =
{
	Asset("ANIM", "anim/warty.zip"),
	Asset("ANIM", "anim/ghost_warty_build.zip"),
}

-- Define skins for the character
local skins =
{
	normal_skin = "warty",              -- Normal skin for the character
	ghost_skin = "ghost_warty_build",   -- Ghost skin for the character
}

-- Create the prefab skin for the character
return CreatePrefabSkin("warty_none",    -- Skin name
		{
			base_prefab = "warty",               -- Base prefab for the skin
			type = "base",                       -- Type of skin
			assets = assets,                     -- Assets used by the skin
			skins = skins,                       -- Skins defined for the character
			skin_tags = {"WARTY", "CHARACTER", "BASE"},  -- Tags associated with the skin
			build_name_override = "warty",        -- Build name override for the skin
			rarity = "Character",                 -- Rarity of the skin
		})
