﻿local ORDERS =
{
    {"intimidating",           { -214+18   , 176 + 30 }},
    {"unintimidating",         { -62       , 176 + 30 }}
}

--------------------------------------------------------------------------------------------------

local function BuildSkillsData(SkillTreeFns)
    local skills =
    {
        warty_intimidating_1 = {
            title = "Intimidating",
            desc = "Makes warty intimidating to creatures.",
            icon = "wilson_alchemy_1",
            pos = {-62,176},
            group = "intimidating",
            tags = {"intimidating"},
            onactivate = function(inst, fromload)
                inst:AddTag("warty_skill_intimidating")
            end,
            root = true,
            connects = {
                "warty_intimidating_2"
            },
        },

        warty_intimidating_2 = {
            title = "Murderous Aura",
            desc = "Accumalate more fear from kills",
            icon = "wilson_alchemy_gem_1",
            pos = {-62, 176-52},
            group = "intimidating",
            tags = {"intimidating"},
            onactivate = function(inst, fromload)
                inst:AddTag("warty_skill_murderousaura")
            end,
        },

        warty_unintimidating_1 = {
            title = "Unintimidating",
            desc = "Makes warty even less intimidating",
            icon = "wilson_torch_time_1",
            pos = {-214,176},
            group = "unintimidating",
            tags = {"unintimidating"},
            onactivate = function(inst, fromload)
                inst:AddTag("warty_skill_unintimidating")
            end,
            root = true,
            connects = {
                "warty_unintimidating_2"
            }
        },

        warty_unintimidating_2 = {
            title = "Safe Aura",
            desc = "warty gains reduced fear from kills",
            icon = "wilson_torch_time_2",
            pos = {-214, 176-38},
            group = "unintimidating",
            tags = {"unintimidating"},
            onactivate = function(inst, fromload)
                inst:AddTag("warty_skill_safeaura")
            end
        },
    }

    return {
        SKILLS = skills,
        ORDERS = ORDERS,
    }
end

--------------------------------------------------------------------------------------------------

return BuildSkillsData