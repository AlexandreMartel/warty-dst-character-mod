-- This information tells other players more about the mod
name = "Warty Version Gars"
description = "A character for Don't Starve Together."
author = "Alexandre Martel-Lafleur"
version = "1.1.1"

-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

-- Compatible with Don't Starve Together
dst_compatible = true

-- Not compatible with Don't Starve
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false

-- Character mods are required by all clients
all_clients_require_mod = true

-- Icon for the mod displayed in the mod menu
icon_atlas = "modicon.xml"
icon = "modicon.tex"

-- The mod's tags displayed on the server list
server_filter_tags = {
    "character",
}

-- Configuration options if needed
-- configuration_options = {}
