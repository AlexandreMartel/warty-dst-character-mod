<a name="readme-top"></a>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📖 About the Project](#about-project)
- [💻 Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Setup the Mod Manually (Mod Tools Required)](#setup-mod-tools)
    - [Optional Steps (Development Only)](#setup-mod-tools-optional-steps)
  - [Setup via Steam Workshop](#setup-steam-workshop)
  - [Usage](#usage)
  - [Run Tests](#run-tests)
- [📝 License](#license)

<!-- PROJECT DESCRIPTION -->

# 📖 Warty - DST Character Mod <a name="about-project"></a>

**Warty - DST Character Mod** is a character mod for the video game Don't Starve Together. Inspired by the DST character Webber, Warty features multiple mechanics with frogs instead of spiders.

<p align="right"><a href="#readme-top">Back to top</a><p>

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

Follow these steps to get a local copy up and running.

### Prerequisites

To run this project, you will need:

  - **Don't Starve Together Mod Tools (Optional)**
  - **Don't Starve Together**

<p align="right"><a href="#readme-top">Back to top</a><p>

### Setup the Mod Manually (Mod Tools Required) <a name="setup-mod-tools"></a>

1. **Install** or **clone** this repository to your desired folder. Extract it if you downloaded it.
2. Copy the folder named `warty`.
3. Paste it into the **Don't Starve Together** `mods` folder.
   - Depending on where you installed the game, the **path** to your mod folder should look like this: `C:\SteamLibrary\steamapps\common\Don't Starve Together\mods`
4. **Launch** the game **Don't Starve Together**.
5. In `Mods > Server Mods`, you should see the mod named `Warty`.

<p align="right"><a href="#readme-top">Back to top</a><p>

#### Optional Steps (Development Only) <a name="setup-mod-tools-optional-steps"></a>

If you want to modify any assets, you will need to delete all the compiled folders each time there is a modification.

I created a script to automate this; it deletes all compiled assets and recompiles them.

To use it, follow these steps:

1. **Add** a new **environment variable**:

| Name              | Value                         | Example (On Windows)                                   |
| ----------------- | ----------------------------- | ------------------------------------------------------ |
| `DONTSTARVE`      | `path-to-mod-tools-folder`    | `C:\SteamLibrary\steamapps\common\Don't Starve Mod Tools` |   

2. **Run** the **delete.bat** file. This should delete all the compiled assets and recompile them.

**Note: If you add new assets, you must update the script to include them.**

<p align="right"><a href="#readme-top">Back to top</a><p>

### Setup via Steam Workshop <a name="setup-steam-workshop"></a>

You can also download the mod via the Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=3232336141

**Note: Some files may be excluded due to the 100MB upload limit. This also improves game performance.**

<p align="right"><a href="#readme-top">Back to top</a><p>

### Usage

Once the setup is complete, enable the mod in the mods section of your **world settings** via `Mods > Server Mods > Warty`.

<p align="right"><a href="#readme-top">Back to top</a><p>

### Run Tests <a name="run-tests"></a>

To execute the tests, you will need to install **Lua** on your machine.

Personnally, im using the version **Lua54**, so take notes that if you are using another version the commands may varies or the tests may not work properly.

If you are on Windows you can follow this tutorial to setup Lua : https://youtu.be/-kdkz6xe-j0

Once this is done, navigate to the `prefabs` folder, open a terminal and run this command :

```
lua54 test_file_name.lua
```

`test_file_name.lua` being the name of the test file you want to run.

<p align="right"><a href="#readme-top">Back to top</a><p>

<!-- LICENSE -->

## 📝 License <a name="license"></a>

This project is licensed under the [MIT License](./LICENSE).

<p align="right"><a href="#readme-top">Back to top</a><p>
